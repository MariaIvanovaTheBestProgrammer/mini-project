import { Injectable } from '@angular/core';
import { AuthenticationService } from './auth.service';
import { Post } from '../models/post/post';
import { Comment } from '../models/comment/comment';


import { NewReaction } from '../models/reactions/newReaction';
import { PostService } from './post.service';
import { User } from '../models/user';
import { map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { Reaction } from '../models/reactions/reaction';

@Injectable({ providedIn: 'root' })
export class LikeService {
    public constructor(private authService: AuthenticationService, private postService: PostService) {}
   
    public dislikePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: false,
            userId: currentUser.id
        };

        // update current array instantly
        innerPost.reactions = this.calculate(innerPost.reactions, currentUser, false);
        
        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = this.calculate(innerPost.reactions, currentUser, false);
                return of(innerPost);
            })
        );
    }
    public likePost(post: Post, currentUser: User) {
        const innerPost = post;

        const reaction: NewReaction = {
            entityId: innerPost.id,
            isLike: true,
            userId: currentUser.id
        };

        innerPost.reactions = this.calculate(innerPost.reactions, currentUser, true);
        
        return this.postService.likePost(reaction).pipe(
            map(() => innerPost),
            catchError(() => {
                // revert current array changes in case of any error
                innerPost.reactions = this.calculate(innerPost.reactions, currentUser, true);
                return of(innerPost);
            })
        );
    }
    calculate(reactions: Reaction[], currentUser: User, isLike: boolean): Reaction[] {
        let hasReaction = reactions.filter((x) => x.user.id === currentUser.id);
        return hasReaction.length == 0
            ? reactions.concat({ isLike: isLike, user: currentUser })
            : hasReaction[0].isLike != isLike
                ? reactions.map((x) => {
                    if ( x.user.id === currentUser.id) {
                        x.isLike = isLike;
                        return x;
                    }
                    else {
                        return x;
                    }
                }) 
                : reactions.filter((x) => x.user.id !== currentUser.id)
    }

    public likeComment(comment : Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: true,
            userId: currentUser.id
        };

        innerComment.reactions = this.calculate(innerComment.reactions, currentUser, true);
        
        return this.postService.likePost(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = this.calculate(innerComment.reactions, currentUser, true);
                return of(innerComment);
            })
        );
    }
    
    public dislikeComment(comment : Comment, currentUser: User) {
        const innerComment = comment;

        const reaction: NewReaction = {
            entityId: innerComment.id,
            isLike: true,
            userId: currentUser.id
        };

        innerComment.reactions = this.calculate(innerComment.reactions, currentUser, false);
        
        return this.postService.likePost(reaction).pipe(
            map(() => innerComment),
            catchError(() => {
                // revert current array changes in case of any error
                innerComment.reactions = this.calculate(innerComment.reactions, currentUser, false);
                return of(innerComment);
            })
        );
    }
}
