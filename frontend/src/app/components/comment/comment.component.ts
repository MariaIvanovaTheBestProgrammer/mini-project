import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { empty } from 'rxjs';
import { Subject } from 'rxjs';
import { catchError, switchMap, takeUntil } from 'rxjs/operators';
import { ConfirmDialogComponent } from 'src/app/confirm-dialog/confirm-dialog.component';
import { DialogType } from 'src/app/models/common/auth-dialog-type';
import { AuthDialogService } from 'src/app/services/auth-dialog.service';
import { AuthenticationService } from 'src/app/services/auth.service';
import { CommentService } from 'src/app/services/comment.service';
import { LikeService } from 'src/app/services/like.service';
import { SnackBarService } from 'src/app/services/snack-bar.service';
import { Comment } from '../../models/comment/comment';
import { User } from '../../models/user';

@Component({
    selector: 'app-comment',
    templateUrl: './comment.component.html',
    styleUrls: ['./comment.component.sass']
})
export class CommentComponent {
    @Input() public comment: Comment;
    @Input() public currentUser: User;
    @Output() deletedComment = new EventEmitter<number>();

    public likesCount = 0;
    public dislikesCount = 0;

    public listOfUsersCommentLikes = "";
    public listOfUsersCommentDislikes = "";
    public showActionButton = false;

    private unsubscribe$ = new Subject<void>();

    public constructor(
        private authService: AuthenticationService,
        private authDialogService: AuthDialogService,
        private likeService: LikeService,
        private commentService: CommentService,
        private snackBarService: SnackBarService,
        private dialog: MatDialog
    ) { }

    public ngOnInit() {
        this.countCommentReactions();
    }
    public ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }
    public countCommentReactions() {
        this.likesCount = this.comment.reactions.filter((x) => x.isLike).length;
        this.dislikesCount = this.comment.reactions.filter((x) => !x.isLike).length;
        this.listOfUsersCommentDislikes = this.comment.reactions.filter((x) => !x.isLike).map((x) => x.user.userName).join(",");
        this.listOfUsersCommentLikes = this.comment.reactions.filter((x) => x.isLike).map((x) => x.user.userName).join(",");
        this.showActionButton = this.currentUser ? this.comment.author.id === this.currentUser.id : false;
    }
    public likeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.likeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((comment) => {
                    this.comment = comment;
                    this.countCommentReactions();
                });

            return;
        }

        this.likeService
            .likeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((comment) => {
                this.comment = comment;
                this.countCommentReactions();
            });
    }

    public dislikeComment() {
        if (!this.currentUser) {
            this.catchErrorWrapper(this.authService.getUser())
                .pipe(
                    switchMap((userResp) => this.likeService.dislikeComment(this.comment, userResp)),
                    takeUntil(this.unsubscribe$)
                )
                .subscribe((post) => {
                    this.comment = post;
                    this.countCommentReactions();
                });

            return;
        }

        this.likeService
            .dislikeComment(this.comment, this.currentUser)
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((post) => {
                this.comment = post;
                this.countCommentReactions();
            });
    }

    private catchErrorWrapper(obs: Observable<User>) {
        return obs.pipe(
            catchError(() => {
                this.openAuthDialog();
                return empty();
            })
        );
    }
    public openAuthDialog() {
        this.authDialogService.openAuthDialog(DialogType.SignIn);
    }

    public deleteComment() {
        const confirmDialog = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: 'Confirm Remove Comment',
                message: 'Are you sure, you want to remove this comment: ' + this.comment.id
            }
        });
        confirmDialog.afterClosed().subscribe(result => {
            if (result === true) {
                this.commentService.deleteComment(this.comment.id)
                    .pipe(takeUntil(this.unsubscribe$))
                    .subscribe((comment) => this.deletedComment.emit(this.comment.id));
            }
        });
    }

}
